# /etc/puppet/manifests/site.pp
filebucket { 'main':
  path   => '/etc/puppet/modules/backup',   # This is required for remote filebuckets.
  server => 'puppet', # Optional; defaults to the configured puppet master.
}

File { backup => main, }


#Node ubuntu.localdomain
node 'ubuntu.localdomain' {
  include sudo
} 
